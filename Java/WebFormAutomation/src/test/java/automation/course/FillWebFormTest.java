package automation.course;

import java.time.Duration;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class FillWebFormTest {
	
	static WebDriver driver;
	
	@BeforeAll
	public static void setUp() {
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}
	
	@Test
	void launchBrowser() {
		driver.get("https://formy-project.herokuapp.com/form");
	}
	
	@Test
	void fillNameField() {
		WebElement firstNameField = driver.findElement(By.id("first-name"));
		firstNameField.clear();
		firstNameField.sendKeys("John");
	}
	
	@Test
	void fillLastName() {
		WebElement lastNameField = driver.findElement(By.id("last-name"));
		lastNameField.clear();
		lastNameField.sendKeys("Smith");
	}
	
	@Test 
	void fillJobTitle(){
		WebElement jobTitle = driver.findElement(By.id("job-title"));
		jobTitle.clear();
		jobTitle.sendKeys("Automation Tester");
	}
	
	@Test
	void fillEducationLevel() {
		WebElement educationLevel = driver.findElement(By.id("radio-button-2"));
		educationLevel.click();
	}
	
	@Test 
	void fillSexSelector(){
		WebElement sexSelector = driver.findElement(By.id("checkbox-1"));
		sexSelector.click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	
	@Test
	void fillExperience() {
		WebElement experience = driver.findElement(By.id("select-menu"));
		experience.click();
		
		WebElement yearsSelector = driver.findElement(By.cssSelector("#select-menu > option:nth-child(3)"));
		yearsSelector.click();
		experience.click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	}
	
	@Test
	void fillDate() {
		WebElement dateField = driver.findElement(By.id("datepicker"));
		dateField.click();
		dateField.sendKeys("03/21/2022",Keys.ENTER);
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
	}
	
	@AfterAll
	public static void submitForm() {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		WebElement submitButton = driver.findElement(By.cssSelector("body > div > form > div > div:nth-child(15) > a"));
		submitButton.click();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.quit();
	}
	
}
