package Tasks;
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;


public class ChangeDateFormat {
	static String convertDateFormat(String inputDate) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy");
		Date date = null;
		try {
			date = sdf.parse(inputDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat outputsdf = new SimpleDateFormat("yyy/mm/dd");
		String outputDate = outputsdf.format(date);
		System.out.println("\n Formatted DOB: " + outputDate);
		return outputDate;
	}
	

}
