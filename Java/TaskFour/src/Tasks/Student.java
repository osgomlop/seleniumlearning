package Tasks;

public class Student {
	
	private int studentId;
	private static String firstName;
	private static String middleName;
	private static String lastName;
	private static String dayOfBirth;
	


	public static String getDayOfBirth() {
		return dayOfBirth;
	}

	public Student(int studentId, String firstName, String middleName, String lastName, String dayOfBirth) {
		this.studentId = studentId;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.dayOfBirth = dayOfBirth;
	}
	
	public static void showStudent(int studentId) {
		System.out.println("\n New student information: " + firstName + " " + middleName + " " + lastName + " StudentID: " + studentId);
	}
	
	

}
