package Tasks;

public class ExceptionsHandling {

	private static final int rand_id = 0;

	public static void main (String args[]) {
				
		try {
			String txt_1 = "Hello";
			String txt_2 = null;
			
			if (txt_1.length()>0 && txt_2.length()>0) {
				System.out.println(txt_1 + txt_2);	
			}
		}
		catch(NullPointerException e) {
				System.out.println("\n Can't concatenate a null value");	
			}
			
		
		try {
			int val_1 = 20;
			int val_2 = 0;
			int result = val_1/val_2;
			System.out.println("The result is: " + result);
		}
		catch(ArithmeticException e) {
			System.out.println("\n Can�t divide by zero");
		}
		
		
		Student Stud1 = new Student(RandomId.RandomId(),"John", "Winston","Lennon","09/10/1940");
		Student.showStudent(RandomId.RandomId());
		ChangeDateFormat.convertDateFormat(Stud1.getDayOfBirth());
				
		Student Stud2 = new Student(RandomId.RandomId(),"John", "Fitzgerald","Kennedy","29/05/1917");
		Student.showStudent(RandomId.RandomId());
		ChangeDateFormat.convertDateFormat(Stud2.getDayOfBirth());
		
		Student Stud3 = new Student(RandomId.RandomId(),"George", "Walker","Bush","06/07/1946");
		Student.showStudent(RandomId.RandomId());
		ChangeDateFormat.convertDateFormat(Stud3.getDayOfBirth());
		
	
		
	}

}

