package webform;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class FillWebForm {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "C:\\Selenium\\chromedriver.exe");
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.get("https://formy-project.herokuapp.com/form");
		
		WebElement firstNameField = driver.findElement(By.id("first-name"));
		firstNameField.sendKeys("John");
		
		WebElement lastNameField = driver.findElement(By.id("last-name"));
		lastNameField.sendKeys("Smith");
		
		WebElement jobTitle = driver.findElement(By.id("job-title"));
		jobTitle.sendKeys("Automation Tester");
		
		WebElement educationLevel = driver.findElement(By.id("radio-button-2"));
		educationLevel.click();
		
		WebElement sexSelector = driver.findElement(By.id("checkbox-1"));
		sexSelector.click();
		
		WebElement experience = driver.findElement(By.id("select-menu"));
		experience.click();
		
		WebElement yearsSelector = driver.findElement(By.cssSelector("#select-menu > option:nth-child(3)"));
		yearsSelector.click();
		experience.click();
		
		WebElement dateField = driver.findElement(By.id("datepicker"));
		dateField.click();
		dateField.sendKeys("03/21/2022",Keys.ENTER);
		
		WebElement submitButton = driver.findElement(By.cssSelector("body > div > form > div > div:nth-child(15) > a"));
		submitButton.click();
		
	
	}

}
